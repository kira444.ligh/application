/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.dao;

import java.io.IOException;
import java.util.List;

/**
 *
 * @author Grethel Gomez
 */
public interface ModelEstudiante <T>{
    void create(T t) throws IOException;
    List<T> showAll() throws IOException;
    boolean delete(T t) throws IOException;
}
