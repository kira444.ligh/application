/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Grethel Gomez
 */
public class Estudiante {
    
    private int id; // 4
    private String carnet; // 23
    private String nombres; // 33
    private String apellidos; // 33
    private String grupo; // 15
    private int notas; // 4

    public Estudiante() {
    }

    public Estudiante(int id, String carnet, String nombres, String apellidos, String grupo, int notas) {
        this.id = id;
        this.carnet = carnet;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.grupo = grupo;
        this.notas = notas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public int getNotas() {
        return notas;
    }

    public void setNotas(int notas) {
        this.notas = notas;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "id=" + id + ", carnet=" + carnet + ", nombres=" + nombres + ", apellidos=" + apellidos + ", grupo=" + grupo + ", notas=" + notas + '}';
    }
    
    public List<String> toList() { // -> para enviar los atributos de cada estudiante a la tabla
        List<String> estudiante = new ArrayList<>();
        
        estudiante.add(String.valueOf(id));
        estudiante.add(carnet);
        estudiante.add(nombres);
        estudiante.add(apellidos);
        estudiante.add(grupo);
        estudiante.add(String.valueOf(notas));
        
        return estudiante;
    }
    
}
