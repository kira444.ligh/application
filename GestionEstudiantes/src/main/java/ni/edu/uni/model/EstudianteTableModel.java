/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import ni.edu.uni.entities.Estudiante;

/**
 *
 * @author Grethel Gomez
 */
public class EstudianteTableModel extends AbstractTableModel{

    private List<String> columnNames;
    private List<Estudiante> data;
    private EstudianteImplements estudiantes;
    
    public EstudianteTableModel() {
        super();
        columnNames = new ArrayList<>();
        data = new ArrayList<>();
        estudiantes = new EstudianteImplements();
    }
    
    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (data.isEmpty()) {
            return null;
        }

        if (rowIndex < 0 || rowIndex >= data.size()) {
            return null;
        }

        List<String> estudiante = data.get(rowIndex).toList();

        if (columnIndex < 0 || columnIndex >= estudiante.size()) {
            return null;
        }

        return estudiante.get(columnIndex);
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {
        if (row < 0 || row >= data.size()) {
            return;
        }
        List<String> rowEst = data.get(row).toList();
        if (col < 0 || col >= rowEst.size()) {
            return;
        }

        rowEst.set(col, value.toString());
        fireTableCellUpdated(row, col);
    }

    @Override
    public String getColumnName(int column) {
        return columnNames.get(column);
    }

    public int addRow() {
        return addRow(new Estudiante());
    }
    
    public int addRow(Estudiante row) {
        data.add(row);
        fireTableRowsInserted(data.size() - 1, data.size() - 1);
        return data.size() - 1;
    }

    public void deleteRow(int row) {
        if (row < 0 || row >= data.size()) {
            return;
        }

        try {
            estudiantes.delete(data.get(row));
        } catch (IOException ex) {
            Logger.getLogger(EstudianteTableModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        data.remove(row);
        fireTableRowsDeleted(row, row);
    }
    
    public void loadFromFile() throws IOException {

        data = estudiantes.showAll();

        String[] names = {"ID", "Carnet", "Nombres", "Apellidos",
            "Grupo", "Nota"};
        columnNames = Arrays.asList(names);
    }
    
}
