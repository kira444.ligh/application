/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.model;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ni.edu.uni.dao.ModelEstudiante;
import ni.edu.uni.entities.Estudiante;

/**
 *
 * @author Grethel Gomez
 */
public class EstudianteImplements implements ModelEstudiante<Estudiante> {

    private File fHead;
    private File fData;
    private RandomAccessFile rafHead;
    private RandomAccessFile rafData;
    private final int SIZE = 112;
    
    private void open() throws IOException {
        fHead = new File("estudiantes.data");
        fData = new File("estudiantes.dat");

        if (!fHead.exists()) {
            fHead.createNewFile();
            rafHead = new RandomAccessFile(fHead, "rw");
            rafData = new RandomAccessFile(fData, "rw");
            rafHead.seek(0);
            rafHead.writeInt(0);
            rafHead.writeInt(0);
        } else {
            rafHead = new RandomAccessFile(fHead, "rw");
            rafData = new RandomAccessFile(fData, "rw");
        }
    }

    private void close() throws IOException {
        if (rafHead != null) {
            rafHead.close();
        }
        if (rafData != null) {
            rafData.close();
        }
    }
    
    @Override
    public void create(Estudiante t) throws IOException {
        open();
        rafHead.seek(0);
        int n = rafHead.readInt();
        int k = rafHead.readInt();

        long pos = k * SIZE;

        rafData.seek(pos);

        rafData.writeInt(++k);
        rafData.writeUTF(t.getCarnet());
        rafData.writeUTF(t.getNombres());
        rafData.writeUTF(t.getApellidos());
        rafData.writeUTF(t.getGrupo());
        rafData.writeInt(t.getNotas());

        rafHead.seek(0);
        rafHead.writeInt(++n);
        rafHead.writeInt(k);

        long hpos = 8 + 4 * (n - 1);
        rafHead.seek(hpos);
        rafHead.writeInt(k);
        close();
    }

    @Override
    public List<Estudiante> showAll() throws IOException {
        open();
        List<Estudiante> estudiantes = new ArrayList<>();
        rafHead.seek(0);
        int n = rafHead.readInt();

        for (int i = 0; i < n; i++) {
            long hpos = 8 + 4 * i;
            rafHead.seek(hpos);

            int index = rafHead.readInt();
            long dpos = (index - 1) * SIZE;
            rafData.seek(dpos);

            Estudiante e = new Estudiante();
            e.setId(rafData.readInt());
            e.setCarnet(rafData.readUTF());
            e.setNombres(rafData.readUTF());
            e.setApellidos(rafData.readUTF());
            e.setGrupo(rafData.readUTF());
            e.setNotas(rafData.readInt());

            estudiantes.add(e);
        }
        close();
        return estudiantes;
    }

    @Override
    public boolean delete(Estudiante t) throws IOException {
        File tmp = new File("tmp.dat");
        try (RandomAccessFile tmpraf = new RandomAccessFile(tmp, "rw")) {
            open();
            rafHead.seek(0);
            int n = rafHead.readInt();
            int k = rafHead.readInt();

            tmpraf.seek(0);
            tmpraf.writeInt(n - 1);
            tmpraf.writeInt(k);
            int j = 0;
            for (int i = 0; i < n; i++) {
                long hpos = 8 + 4 * i;
                rafHead.seek(hpos);
                int id = rafHead.readInt();
                if (id == t.getId()) {
                    continue;
                }
                long tmpos = 8 + 4 * j++;
                tmpraf.seek(tmpos);
                tmpraf.writeInt(id);
            }
            close();
        }
        close();
        File f = new File("estudiante.data");
        boolean flag = f.delete();
        if (flag) {
            tmp.renameTo(f);
        } else {
            Logger.getLogger(Estudiante.class.getName()).log(Level.SEVERE, "ERROR, no se pudo eliminar el archivo!");
        }
        return flag;
    }
    
}
